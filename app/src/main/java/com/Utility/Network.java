package com.Utility;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Shenll Technology Solutions
 * Network is the utility class to check internet connection available or not.
 */
public class Network {

	/**
	 * This method will check network connection exists or not using NetworkInfo class
	 * @param thisActivity
	 * @return
	 */
	public static boolean haveInternet(Activity thisActivity) {
		NetworkInfo info = ((ConnectivityManager) thisActivity
				.getSystemService(Context.CONNECTIVITY_SERVICE))
				.getActiveNetworkInfo();

		if (info == null || !info.isConnected()) {
			return false;
		}
		if (info.isRoaming()) {
			return true;
		}
		return true;
	}

}
