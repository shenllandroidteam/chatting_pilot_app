package com.Utility;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Shenll Technology Solutions
 * PreferencesManager is the helper class for shared preference temporary storage.
 */
public class PreferencesManager {

    // TODO: CHANGE THIS TO SOMETHING MEANINGFUL
    private static final String SETTINGS_NAME = "default_settings";
    private static PreferencesManager sSharedPrefs;
    private SharedPreferences mPref;

    /**
     * Constructor
     * @param context
     */
    private PreferencesManager(Context context) {
        mPref = context.getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);
    }

    /**
     * Singleton method that will return the instance of this class
     * @param context
     * @return
     */
    public static PreferencesManager getInstance(Context context) {
        if (sSharedPrefs == null) {
            sSharedPrefs = new PreferencesManager(context.getApplicationContext());
        }
        return sSharedPrefs;
    }

}
