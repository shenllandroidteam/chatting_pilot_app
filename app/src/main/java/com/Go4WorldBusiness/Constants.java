package com.Go4WorldBusiness;

/**
 * Shenll Technology Solutions
 * Constants used by this chatting application.
 * TODO: Replace PUBLISH_KEY and SUBSCRIBE_KEY with your personal keys.
 * TODO: Register app for GCM and replace GCM_SENDER_ID
 */
public class Constants {
        //our key
    public static final String PUBLISH_KEY   = "pub-c-64711acd-4eb9-4e66-85bc-3140d33c0bbc";
    public static final String SUBSCRIBE_KEY = "sub-c-c708e4d2-8aec-11e6-8c91-02ee2ddab7fe";

    // new key
    /*public static final String PUBLISH_KEY   = "pub-c-0f68103e-4a40-4371-bfe9-9ee8f99ab718";
    public static final String SUBSCRIBE_KEY = "sub-c-4feeef00-95f9-11e6-a681-02ee2ddab7fe";*/

    // git key
    /*public static final String PUBLISH_KEY   = "pub-c-7fad26fe-6c38-4940-b9c3-fbd19a9633af";
    public static final String SUBSCRIBE_KEY = "sub-c-30c17e1a-0007-11e5-a8ef-0619f8945a4f";*/

    public static final String DEMO_URL   = "http://demo.shenll.net/go4worldbusiness/api.php?";
    public static final String API_REQUEST_TAG_TYPE = "type";
    public static final String API_REQUEST_LOGIN = "login";
    public static final String API_REQUEST_USRNAME = "username";
    public static final String API_REQUEST_PASSWORD  = "password";


    public static final String API_RESPONSE_CODE = "responseCode";
    public static final String API_RESPONSE_MESSAGE = "message";
    public static final String API_RESPONSE_CONTACT_USER = "users";
    public static final String API_RESPONSE_CONTACT_NAME = "name";
    public static final String API_RESPONSE_CONTACT_PASSWORD = "password";
    public static final String API_RESPONSE_CONTACT_IMAGE = "image";


    public static final String USER_IMAGE = "http://demo.shenll.net/App/user.png";
    public static final String USER_IMAGE_ONE = "http://demo.shenll.net/App/images_1.jpg";
    public static final String USER_IMAGE_TWO = "http://demo.shenll.net/App/images_2.jpg";
    public static final String USER_IMAGE_THREE = "http://demo.shenll.net/App/images_3.jpg";

    public static final String CHAT_PREFS    = "com.Go4WorldBusiness.SHARED_PREFS";
    public static final String CHAT_USERNAME = "com.Go4WorldBusiness.SHARED_PREFS.USERNAME";
    public static final String CHAT_PASSWORD = "com.Go4WorldBusiness.SHARED_PREFS.PASSWORD";
    public static final String CHAT_ROOM     = "com.Go4WorldBusiness.CHAT_ROOM";

    //public static final String JSON_GROUP = "groupMessage";
    public static final String JSON_DM    = "directMessage";
    public static final String JSON_USER  = "chatUser";
    public static final String JSON_MSG   = "chatMsg";
    public static final String JSON_TIME  = "chatTime";
    public static final String JSON_OWNER  = "chatOwner";

    public static final String STATE_LOGIN = "loginTime";
    public static final String STATE_LOGIN_PASS = "password";
    public static final String STATE_LOGIN_USER_IMAGE = "userImage";


    public static final String GCM_REG_ID    = "gcmRegId";
    public static final String GCM_SENDER_ID = "313615544662"; // Get this from
    public static final String GCM_POKE_FROM = "gcmPokeFrom"; // Get this from
    public static final String GCM_CHAT_ROOM = "gcmChatRoom"; // Get this from
    public final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

}
