package com.Go4WorldBusiness.Controller.Interface;

import com.Go4WorldBusiness.Model.Contact;
import com.Go4WorldBusiness.View.ViewHolder.ContactListHolder;

public interface OnContactUserSelectListener {
    /**
     * This method will interact between the activity and the list adapter to track selected user.
     * @param tasksHolder
     * @param names
     * @param position
     */
    void onContactUserSelect(ContactListHolder tasksHolder, Contact names, int position);
}
