package com.Go4WorldBusiness.Controller.Interface;

import com.Go4WorldBusiness.Model.User;
import com.Go4WorldBusiness.View.ViewHolder.UserListHolder;

public interface OnUserSelectListener {
    /**
     * This method will interact between the activity and the list adapter to track selected user.
     * @param tasksHolder
     * @param names
     * @param position
     */
    void onUserSelect(UserListHolder tasksHolder, User names, int position);
}
