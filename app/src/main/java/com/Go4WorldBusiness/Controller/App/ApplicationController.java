package com.Go4WorldBusiness.Controller.App;

import android.app.Application;
import android.content.res.Configuration;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Shenll Technology Solutions
 * ApplicationController is the launching area of the Application.
 * This class extends the behaviour of Application class.
 */
public class ApplicationController extends Application {

    public static final String TAG = ApplicationController.class.getSimpleName();
    private static ApplicationController mInstance;
    private RequestQueue mRequestQueue;

    @Override
    public void onCreate() {
        super.onCreate();
        /*Thread.setDefaultUncaughtExceptionHandler(new DebuggingUncaughtExceptionHandler(
                getApplicationContext(),
                Thread.getDefaultUncaughtExceptionHandler()));*/
        mInstance = this;
    }

    public static synchronized ApplicationController getInstance() {
        return mInstance;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

}
