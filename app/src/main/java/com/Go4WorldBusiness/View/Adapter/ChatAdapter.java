package com.Go4WorldBusiness.View.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.Go4WorldBusiness.Model.ChatMessage;
import com.Go4WorldBusiness.R;

/**
 * Shenll Technology Solutions
 * Custom adapter that takes a list of ChatMessages and fills a chat_owner_row_layout.xml view with the ChatMessage's information.
 * This class extends ArrayAdapter
 */
public class ChatAdapter extends ArrayAdapter<ChatMessage> {

    private final Context context;
    private LayoutInflater inflater;
    private List<ChatMessage> values;
    private Set<String> onlineNow = new HashSet<String>();
    private String loggedUser ="";
    Typeface customFont;

    /**
     * Constructor
     * @param context
     * @param values
     * @param loggedUser
     */
    public ChatAdapter(Context context, List<ChatMessage> values,String loggedUser) {
        super(context, R.layout.chat_owner_row_layout, android.R.id.text1, values);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.values=values;
        this.loggedUser=loggedUser;
    }

    /**
     * ViewHolder for the ChatAdapter
     */
    class ViewHolder {
        TextView message;
        TextView timeStamp;
        LinearLayout lyt_right, lyt_set_background;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ChatMessage chatMsg = this.values.get(position);
        ViewHolder holder;
        if (convertView == null) {

            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.chat_owner_row_layout, parent, false);
            customFont = Typeface.createFromAsset(this.context.getAssets(),"Roboto-Regular.ttf");

            holder.message = (TextView) convertView.findViewById(R.id.chat_message);
            holder.timeStamp = (TextView) convertView.findViewById(R.id.chat_time);
            holder.lyt_right = (LinearLayout)convertView.findViewById(R.id.lyt_right);
            holder.lyt_set_background = (LinearLayout)convertView.findViewById(R.id.lyt_set_background);
            holder.message.setTypeface(customFont);
            holder.timeStamp.setTypeface(customFont);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.message.setText(chatMsg.getMessage());
        holder.timeStamp.setText(formatTimeStamp(chatMsg.getTimeStamp()));

        if(chatMsg.getChatOwner().equals(this.loggedUser)){
            holder.message.setGravity(Gravity.RIGHT);
            holder.timeStamp.setGravity(Gravity.RIGHT);
            holder.lyt_right.setGravity(Gravity.RIGHT);
            holder.lyt_set_background.setBackgroundResource(R.drawable.round_owner_rect_shape);
        }
        else{
            holder.message.setGravity(Gravity.LEFT);
            holder.timeStamp.setGravity(Gravity.LEFT);
            holder.lyt_right.setGravity(Gravity.LEFT);
            holder.lyt_set_background.setBackgroundResource(R.drawable.round_rect_shape);
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return this.values.size();
    }
    /**
     * Method to add a single message and update the listview.
     * @param chatMsg Message to be added
     */
    public void addMessage(ChatMessage chatMsg){
        this.values.add(chatMsg);
        notifyDataSetChanged();
    }

    /**
     * This method will return chat history messages
     */
    public List<ChatMessage> getMessages(){
        return this.values;
    }

    /**
     * Method to add a list of messages and update the listview.
     * @param chatMsgs Messages to be added
     */
    public void setMessages(List<ChatMessage> chatMsgs){
        this.values.clear();
        this.values.addAll(chatMsgs);
        notifyDataSetChanged();
    }

    /**
     * Handle users. Fill the onlineNow set with current users. Data is used to display a green dot
     *   next to users who are currently online.
     * @param user UUID of the user online.
     * @param action The presence action
     */
    public void userPresence(String user, String action){
        boolean isOnline = action.equals("join") || action.equals("state-change");
        if (!isOnline && this.onlineNow.contains(user))
            this.onlineNow.remove(user);
        else if (isOnline && !this.onlineNow.contains(user))
            this.onlineNow.add(user);
        notifyDataSetChanged();
    }

    /**
     * Overwrite the onlineNow array with all the values attained from a call to hereNow().
     * @param onlineNow
     */
    public void setOnlineNow(Set<String> onlineNow){
        this.onlineNow = onlineNow;
        notifyDataSetChanged();
    }

    /**
     * Format the long System.currentTimeMillis() to a better looking timestamp. Uses a calendar
     *   object to format with the user's current time zone.
     * @param timeStamp
     * @return
     */
    public static String formatTimeStamp(long timeStamp){
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm a");

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp);
        return formatter.format(calendar.getTime());
    }


}
