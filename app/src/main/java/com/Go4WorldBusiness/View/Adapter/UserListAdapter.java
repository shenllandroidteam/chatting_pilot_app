package com.Go4WorldBusiness.View.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.Go4WorldBusiness.Model.User;
import com.Go4WorldBusiness.R;
import com.squareup.picasso.Picasso;
import com.Go4WorldBusiness.Controller.Interface.OnUserSelectListener;
import com.Go4WorldBusiness.View.CropTheCircle;
import com.Go4WorldBusiness.View.ViewHolder.UserListHolder;
import com.Go4WorldBusiness.Model.ChatMessage;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Shenll Technology Solutions
 * Custom adapter that takes a list of Users displaying username, image, last sent messages and time.
 * This class extends RecyclerView.Adapter
 */

public class UserListAdapter extends RecyclerView.Adapter{

    Context context;
    List<User> temp;
    List<User> itemsList;
    private OnUserSelectListener onUSerSelect;
    private final int VIEW_ITEM = 1;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    Typeface customFont;

    /**
     * Constructor
     * @param context
     * @param itemsList
     * @param recyclerView
     */
    public UserListAdapter(final Context context, List<User> itemsList, RecyclerView recyclerView) {
        this.context = context;
        this.temp=itemsList;
        this.itemsList = itemsList;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                           super.onScrollStateChanged(recyclerView, newState);
                        }

                        @Override
                        public void onScrolled(RecyclerView recyclerView,int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold))
                                    loading = true;
                        }
                    });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_ITEM;
    }

    @Override
    public int getItemCount() {
        if (itemsList == null)
            return 0;
        else
            return itemsList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {

        RecyclerView.ViewHolder vh;
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.add_user_list, viewGroup, false);
            customFont = Typeface.createFromAsset(viewGroup.getContext().getAssets(),"Roboto-Regular.ttf");
            vh = new UserListHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof UserListHolder) {
            final User singleTasks = (User) itemsList.get(position);
            ((UserListHolder) holder).context = context;
            ((UserListHolder) holder).textView.setTypeface(customFont);

            ((UserListHolder) holder).textView.setText(singleTasks.getName());

            List<ChatMessage> chatMessages = singleTasks.getChatMessages();

            if(chatMessages != null && chatMessages.size()>0) {
                ChatMessage lastChat = chatMessages.get(chatMessages.size()-1);
                ((UserListHolder) holder).userChatMessage.setText(lastChat.getMessage());
                ((UserListHolder) holder).userChatTime.setText("" +formatTimeStamp(lastChat.getTimeStamp()));
                ((UserListHolder) holder).imageOnlineTick.setVisibility(View.VISIBLE);
            }

            Picasso.with(context).load(singleTasks.getImage())
                    .error(R.drawable.user)
                    .transform(new CropTheCircle()).into(((UserListHolder) holder).imageView);

            ((UserListHolder) holder).layoutRlyMain.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    onUSerSelect.onUserSelect(((UserListHolder) holder), singleTasks, position);
                }
            });
        }
    }

    /**
     * This method will set the listener context from activity to track user selection
     * @param onUSerSelect
     */
    public void setOnUserSelectListener(OnUserSelectListener onUSerSelect) {
        this.onUSerSelect = onUSerSelect;
    }

    /**
     * Handle users. Fill the onlineNow set with current users. Data is used to display a green dot
     *   next to users who are currently online.
     * @param user UUID of the user online.
     * @param action The presence action
     */
    public void userPresence(String user, String action){

        boolean isOnline = action.equals("join") || action.equals("state-change");

        for(int k = 0; k < this.itemsList.size(); k++ ) {
            User userOnline = this.itemsList.get(k);
            if (!isOnline && userOnline.getName().contains(user)) {
                String strUser = userOnline.getName();
                this.itemsList.remove(strUser);
            } else if (isOnline && !userOnline.getName().contains(user)) {
                userOnline.setName(user);
                this.itemsList.add(userOnline);
            }
        }
        notifyDataSetChanged();
    }

    /**
     *
     * @param itemsList
     */
    public void setOnlineNow(List<User> itemsList){
        this.itemsList = itemsList;
        notifyDataSetChanged();
    }

    /**
     * This method will convert time from long to hh:mm a
     * @param timeStamp
     * @return
     */
    public static String formatTimeStamp(long timeStamp){
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm a");
        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp);
        return formatter.format(calendar.getTime());
    }
}