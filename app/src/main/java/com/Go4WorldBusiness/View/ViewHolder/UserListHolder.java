package com.Go4WorldBusiness.View.ViewHolder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.Go4WorldBusiness.R;

/**
 * Shenll Technology Solutions
 * UserListHolder will hold the individual user list.
 * This method implements RecyclerView.ViewHolder class
 */

public class UserListHolder extends RecyclerView.ViewHolder {

    public TextView textView;
    public ImageView imageView, imageOnlineTick;
    public View notesView;
    public Context context;
    public RelativeLayout layoutRlyMain;
    public TextView userChatMessage;
    public TextView userChatTime;

    /**
     * This method will return the view of each individual user
     * @param view
     */
    public UserListHolder(View view) {
        super(view);
        this.notesView=view;
        this.textView = (TextView) view.findViewById(R.id.title);
        this.imageView = (ImageView) view.findViewById(R.id.image);
        this.imageOnlineTick = (ImageView) view.findViewById(R.id.online_tick);
        this.layoutRlyMain = (RelativeLayout) view.findViewById(R.id.layout_rly_main);
        this.userChatMessage = (TextView) view.findViewById(R.id.user_chat_message);
        this.userChatTime = (TextView) view.findViewById(R.id.user_chat_time);
    }
}