package com.Go4WorldBusiness.View.ViewHolder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.Go4WorldBusiness.R;

/**
 * Shenll Technology Solutions
 * ContactListHolder will hold the individual user list.
 * This method extends RecyclerView.ViewHolder class
 */

public class ContactListHolder extends RecyclerView.ViewHolder {

    public TextView textView;
    public ImageView imageView, imageOnlineTick;
    public View contactView;
    public Context context;
    public RelativeLayout layoutRlyMain;
    public TextView userOnline;

    /**
     * This method will return the view of each individual user
     * @param view
     */
    public ContactListHolder(View view) {
        super(view);
        this.contactView=view;
        this.textView = (TextView) view.findViewById(R.id.contact_title);
        this.imageView = (ImageView) view.findViewById(R.id.contact_image);
        this.imageOnlineTick = (ImageView) view.findViewById(R.id.online_tick);
        this.layoutRlyMain = (RelativeLayout) view.findViewById(R.id.layout_rly_main);
        this.userOnline = (TextView) view.findViewById(R.id.user_online);
    }
}