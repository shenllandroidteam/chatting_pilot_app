package com.Go4WorldBusiness.View.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.Go4WorldBusiness.Controller.App.ApplicationController;
import com.Go4WorldBusiness.Controller.Network.WebserviceRequest;
import com.Go4WorldBusiness.Model.Contact;
import com.Go4WorldBusiness.Model.UserContactList;
import com.Go4WorldBusiness.R;
import com.Go4WorldBusiness.View.CropTheCircle;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.google.gson.JsonIOException;
import com.squareup.picasso.Picasso;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.Go4WorldBusiness.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Login Activity for the first time the app is opened, or when a user clicks the sign out button.
 * Saves the username in SharedPreferences.
 */
public class LoginActivity extends Activity {

    private EditText mUsername, mPassword;
    Typeface customFont;
    Button meSignInButton;
    private static MaterialDialog progressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        customFont = Typeface.createFromAsset(LoginActivity.this.getAssets(), "Roboto-Regular.ttf");
        mUsername = (EditText) findViewById(R.id.login_username);
        mPassword = (EditText) findViewById(R.id.login_password);
        meSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mUsername.setTypeface(customFont);
        mPassword.setTypeface(customFont);
        meSignInButton.setTypeface(customFont);



        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String lastUsername = extras.getString("oldUsername", "");
            mUsername.setText(lastUsername);
        }

        createProgressDialogObject();

        mPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {

                    QueueValidation();
                }
                return false;
            }
        });

    }

    /**
     * @param view Button clicked to trigger call to joinChat
     */
    public void joinChat(View view) {
        QueueValidation();
    }

    /**
     * @param password
     * @return
     */
    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Takes the username and password from the EditText, check its validity and saves it if valid.
     * Then, redirects to the MainActivity.
     */
    public void QueueValidation() {

        String name = mUsername.getText().toString();
        String password = mPassword.getText().toString();
        if (name.isEmpty()) {
            AlertDialog.Builder csDialog = new AlertDialog.Builder(LoginActivity.this);
            csDialog.setMessage("" + getResources().getString(R.string.alt_name));
            csDialog.setPositiveButton(getResources().getString(R.string.alt_ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mUsername.requestFocus();
                }
            });
            csDialog.show();
        } else if (!isPasswordValid(password)) {
            AlertDialog.Builder csDialog = new AlertDialog.Builder(LoginActivity.this);
            csDialog.setMessage("" + getResources().getString(R.string.alt_vaild_pass));
            csDialog.setPositiveButton(getResources().getString(R.string.alt_ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mPassword.requestFocus();
                }
            });
            csDialog.show();
        } else {

            SharedPreferences sp = getSharedPreferences(Constants.CHAT_PREFS, MODE_PRIVATE);
            SharedPreferences.Editor edit = sp.edit();
            edit.putString(Constants.CHAT_USERNAME, name);
            edit.putString(Constants.CHAT_PASSWORD, password);
            edit.apply();

            progressDialog.show();
            moveToNextActivity();
        }
    }

    public void moveToNextActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                progressDialog.dismiss();
                Toast.makeText(LoginActivity.this, "Authentication success", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(LoginActivity.this, UserListActivity.class);
                startActivity(intent);
                finish();
            }
        }, 1500);
    }

    public void createProgressDialogObject() {
        progressDialog = new MaterialDialog.Builder(LoginActivity.this)
                .title(R.string.app_name)
                .content(R.string.loading)
                .progress(true, 0)
                .build();

        progressDialog.setCancelable(false);
    }

}