package com.Go4WorldBusiness.View.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.Go4WorldBusiness.Controller.Interface.OnContactUserSelectListener;
import com.Go4WorldBusiness.Model.Contact;
import com.Go4WorldBusiness.Model.User;
import com.Go4WorldBusiness.Model.UserContactList;
import com.Go4WorldBusiness.Model.UserList;
import com.Go4WorldBusiness.R;
import com.Go4WorldBusiness.View.Adapter.ContactListAdapter;
import com.Go4WorldBusiness.View.ViewHolder.ContactListHolder;
import java.util.List;
import com.Go4WorldBusiness.Constants;

/**
 * ContactListActivity is where all registered users will be displayed. A RecycleView that is populated by a custom adapter, ContactListAdapter.
 * This method extends the Activity
 */
public class ContactListActivity extends Activity {

    public String username;
    private LinearLayout mContactArrow;

    List<Contact> singleContact;
    UserContactList allContact;
    UserList allUserList;
    List<User> allUser;
    private SharedPreferences mSharedPrefs;
    private ContactListAdapter adapter;
    private RecyclerView recyclerView;
    private static final int REQUEST_CHAT = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);

        Bundle extras = getIntent().getExtras();

        if (extras != null){
            if (extras.containsKey("Contact"))
            {
                allContact = this.getIntent().getExtras().getParcelable("Contact");
            }
            singleContact = allContact.getAllContact();
            if (extras.containsKey("UserName"))
            {
                allUserList = this.getIntent().getExtras().getParcelable("UserName");
            }
            allUser = allUserList.getAllUser();
        }

        // Init design element objects using its ids
        this.recyclerView = (RecyclerView)findViewById(R.id.recyclerContactView);
        this.mContactArrow = (LinearLayout) findViewById(R.id.lyt_contact_image_arrow);


        mSharedPrefs = getSharedPreferences(Constants.CHAT_PREFS, MODE_PRIVATE);
        this.username = mSharedPrefs.getString(Constants.CHAT_USERNAME, "Anonymous");

        // Set UserList Adapter
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        //adapter = new ContactListAdapter(this, singleContact, allUser, recyclerView);
        adapter = new ContactListAdapter(this, singleContact, recyclerView);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        adapter.setOnContactUserSelectListener(new OnContactUserSelectListener() {
            @Override
            public void onContactUserSelect(ContactListHolder tasksHolder, Contact names, int position) {

                String selecteduserforchat = names.getName();
                String selectedUserImage = names.getImage();
                if(selecteduserforchat.equals(names.getOnlineuser())){
                    Intent intent = new Intent(ContactListActivity.this, ChatActivity.class);
                    intent.putExtra("channel", "Go4WorldBusiness");
                    intent.putExtra("selectedUsernameForChat", selecteduserforchat);
                    intent.putExtra("selectedUserImage", selectedUserImage);
                    startActivityForResult(intent, REQUEST_CHAT);
                    //Toast.makeText(ContactListActivity.this, " is not online."+names.getName(), Toast.LENGTH_SHORT).show();
                }
            }

        });

        this.mContactArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK, null);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        setResult(RESULT_OK, null);
        finish();

    }

}
