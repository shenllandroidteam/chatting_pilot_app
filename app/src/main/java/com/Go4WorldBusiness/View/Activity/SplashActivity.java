package  com.Go4WorldBusiness.View.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import com.Go4WorldBusiness.Constants;
import com.Go4WorldBusiness.R;

/**
 * Shenll Technology Solutions
 * SplashActivity is the launching screen which holds company logo for few seconds
 * This class extends AppCompatActivity
 */
public class SplashActivity extends AppCompatActivity {

    private SharedPreferences mSharedPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mSharedPrefs = getSharedPreferences(Constants.CHAT_PREFS, MODE_PRIVATE);
        moveToNextActivity();
    }

    /**
     * This method will display app logo and hold the screen for 1 second
     * Then navigate to
     */
    public void moveToNextActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!mSharedPrefs.contains(Constants.CHAT_USERNAME)){
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    Intent intent = new Intent(SplashActivity.this, UserListActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, 1000);
    }
}

