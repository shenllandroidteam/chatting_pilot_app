package com.Go4WorldBusiness.View.Activity;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.Go4WorldBusiness.Model.ChatMessage;
import com.Go4WorldBusiness.Model.User;
import com.Go4WorldBusiness.R;
import com.pubnub.api.Callback;
import com.pubnub.api.PnGcmMessage;
import com.pubnub.api.PnMessage;
import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubError;
import com.pubnub.api.PubnubException;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.Go4WorldBusiness.View.Adapter.ChatAdapter;
import com.Go4WorldBusiness.Controller.Callbacks.BasicCallback;
import com.Go4WorldBusiness.Constants;
import com.Go4WorldBusiness.View.CropTheCircle;

/**
 * ChatActivity is where chatting will occur. A Listview that is populated by a custom adapter, ChatAdapter.
 * This method extends the AppCompatActivity
 */

public class ChatActivity extends ListActivity {

    public String username;
    public String selecteduserforchat = "";
    public String userImage = "";
    private Pubnub mPubNub;
    private ListView mListView;
    private ChatAdapter mChatAdapter;
    private SharedPreferences mSharedPrefs;
    private String channel  = "Go4WorldBusiness";
    private LinearLayout  ltyImageArrow;
    private ImageView  imgUser;
    private TextView  titUserName;
    private EditText mMessageET;
    Typeface customFont;

    ChatMessage finalChatMsg = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        // Init design element objects using its ids
        ltyImageArrow = (LinearLayout) findViewById(R.id.lyt_image_arrow);
        titUserName = (TextView) findViewById(R.id.title_user_name);
        imgUser = (ImageView) findViewById(R.id.user_image);
        this.mMessageET = (EditText) findViewById(R.id.message_et);

        // Set custom font
        customFont = Typeface.createFromAsset(ChatActivity.this.getAssets(), "Roboto-Regular.ttf");

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            Log.d("Main-bundle",extras.toString() +"channel" + extras.getString("channel"));
            Log.d("Main-bundle",extras.toString() +"selectedUserImage" + extras.getString("selectedUserImage"));
            Log.d("Main-bundle", extras.toString() + "selectedUsernameForChat" + extras.getString("selectedUsernameForChat"));

            if (extras.containsKey("channel")){
                this.channel = extras.getString("channel");
            }

            if (extras.containsKey("selectedUsernameForChat")){
                this.selecteduserforchat = extras.getString("selectedUsernameForChat");
            }
            if (extras.containsKey("selectedUserImage")){
                this.userImage = extras.getString("selectedUserImage");
            }

            titUserName.setText(""+this.selecteduserforchat);
            titUserName.setTypeface(customFont);
        }

        // Get Chat Owner username from Shared preference and navigate to Login screen if username is empty
        mSharedPrefs = getSharedPreferences(Constants.CHAT_PREFS, MODE_PRIVATE);
        this.username = mSharedPrefs.getString(Constants.CHAT_USERNAME, "Anonymous");

        this.mListView = getListView();

        Picasso.with(ChatActivity.this).load(this.userImage)
                .error(R.drawable.user)
                .transform(new CropTheCircle()).into(imgUser);

        ChatActivity.this.mChatAdapter = new ChatAdapter(ChatActivity.this, new ArrayList<ChatMessage>(), this.username);
        ChatActivity.this.mChatAdapter.userPresence(ChatActivity.this.username, "join"); // Set user to online. Status changes handled in presence
        setupAutoScroll();
        ChatActivity.this.mListView.setAdapter(mChatAdapter);
        setupListView();

        ltyImageArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent();

                if(finalChatMsg!=null){
                    //
                    intent.putExtra("NAME",finalChatMsg.getUsername());
                    intent.putExtra("MESSAGE",finalChatMsg.getMessage());
                    intent.putExtra("TIMESTAMP",finalChatMsg.getTimeStamp());
                }else{
                    intent.putExtra("NAME","");
                    intent.putExtra("MESSAGE","");
                    intent.putExtra("TIMESTAMP",0);
                }

                setResult(RESULT_OK, intent);
                finish();

            }
        });


        this.mMessageET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    sendChatMessage();
                }
                return false;
            }
        });

        initPubNub();
    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent();
        if(finalChatMsg!=null){
            intent.putExtra("NAME",finalChatMsg.getUsername());
            intent.putExtra("MESSAGE",finalChatMsg.getMessage());
            intent.putExtra("TIMESTAMP",finalChatMsg.getTimeStamp());
        }else{
            intent.putExtra("NAME","");
            intent.putExtra("MESSAGE","");
            intent.putExtra("TIMESTAMP",0);
        }


        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // TODO: Update to store messages in the array.
    }

    /**
     * Might want to unsubscribe from PubNub here and create background service to listen while
     *   app is not in foreground.
     * PubNub will stop subscribing when screen is turned off for this demo, messages will be loaded
     *   when app is opened through a call to history.
     * The best practice would be creating a background service in onStop to handle messages.
     */
    @Override
    protected void onStop() {
        super.onStop();
        if (this.mPubNub != null)
            this.mPubNub.unsubscribeAll();
    }

    /**
     * Instantiate PubNub object if it is null. Subscribe to channel and pull old messages via
     *   history.
     */
    @Override
    protected void onRestart() {
        super.onRestart();

        if (this.mPubNub==null){
            initPubNub();
        } else {
            subscribeWithPresence();
            history();

        }
    }

    /**
     * I remove the PubNub object in onDestroy since turning the screen off triggers onStop and
     *   I wanted PubNub to receive messages while the screen is off.
     *
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    /**
     * Instantiate PubNub object with username as UUID
     *   Then subscribe to the current channel with presence.
     *   Finally, populate the listview with past messages from history
     */
    private void initPubNub(){
        this.mPubNub = new Pubnub(Constants.PUBLISH_KEY, Constants.SUBSCRIBE_KEY);
        this.mPubNub.setUUID(this.username);
        Log.v("UUID_ON", "" + this.mPubNub.getUUID());
        subscribeWithPresence();
        history();
    }

    /**
     * Use PubNub to send any sort of data
     * @param type The type of the data, used to differentiate groupMessage from directMessage
     * @param data The payload of the publish
     */
    public void publish(String type, JSONObject data){
        JSONObject json = new JSONObject();
        try {
            json.put("type", type);
            json.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.mPubNub.publish(this.channel, json, new BasicCallback());
    }

    /**
     * Subscribe to channel, when subscribe connection is established, in connectCallback, subscribe
     *   to presence, set login time with setStateLogin and update hereNow information.
     * When a message is received, in successCallback, get the ChatMessage information from the
     *   received JSONObject and finally put it into the listview's ChatAdapter.
     * Chat adapter calls notifyDatasetChanged() which updates UI, meaning must run on UI thread.
     */
    public void subscribeWithPresence(){

        Callback subscribeCallback = new Callback() {
            @Override
            public void successCallback(String channel, Object message) {
                if (message instanceof JSONObject){
                    try {

                        JSONObject jsonObj = (JSONObject) message;
                        JSONObject json = jsonObj.getJSONObject("data");
                        final String name = json.getString(Constants.JSON_USER);
                        String msg  = json.getString(Constants.JSON_MSG);
                        long time   = json.getLong(Constants.JSON_TIME);
                        final  String ownername = json.getString(Constants.JSON_OWNER);
                        if (ownername.equals(mPubNub.getUUID())) return; // Ignore own messages
                        final ChatMessage chatMsg = new ChatMessage(selecteduserforchat, msg, time ,ownername);

                        ChatActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if ((name.equals(selecteduserforchat) && ownername.equals(ChatActivity.this.username))
                                        || (name.equals(ChatActivity.this.username) && ownername.equals(selecteduserforchat))) {

                                    finalChatMsg = chatMsg;
                                    mChatAdapter.addMessage(chatMsg);

                                    for (int k = 0; k < User.allUsersOnline.size(); k++) {
                                        User user = User.allUsersOnline.get(k);
                                        if (user.getName().equals(selecteduserforchat)) {
                                            user.setName(selecteduserforchat);
                                            user.setChatMessages((List<ChatMessage>)mChatAdapter.getMessages());
                                        }
                                    }
                                }
                            }
                        });
                    } catch (JSONException e){ e.printStackTrace(); }
                }
                Log.d("PUBNUB", "Channel: " + channel + " Msg: " + message.toString());
            }

            @Override
            public void connectCallback(String channel, Object message) {
                Log.d("Subscribe", "Connected! " + message.toString());
                hereNow(false);
                setStateLogin();
            }
        };
        try {
            mPubNub.subscribe(this.channel, subscribeCallback);
            presenceSubscribe();
        } catch (PubnubException e){ e.printStackTrace(); }
    }

    /**
     * Subscribe to presence. When user join or leave are detected, update the hereNow number
     *   as well as add/remove current user from the chat adapter's userPresence array.
     *   This array is used to see what users are currently online and display a green dot next
     *   to users who are online.
     */
    public void presenceSubscribe()  {
        Callback callback = new Callback() {
            @Override
            public void successCallback(String channel, Object response) {
                Log.i("PN-pres", "Pres: " + response.toString() + " class: " + response.getClass().toString());
                if (response instanceof JSONObject){
                    JSONObject json = (JSONObject) response;
                    Log.d("PN-main","Presence: " + json.toString());
                    try {
                        final int occ = json.getInt("occupancy");
                        final String user = json.getString("uuid");
                        final String action = json.getString("action");
                        ChatActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mChatAdapter.userPresence(user, action);
                            }
                        });
                    } catch (JSONException e){ e.printStackTrace(); }
                }
            }

            @Override
            public void errorCallback(String channel, PubnubError error) {
                Log.d("Presence", "Error: " + error.toString());
            }
        };
        try {
            this.mPubNub.presence(this.channel, callback);
        } catch (PubnubException e) { e.printStackTrace(); }
    }

    /**
     * Update here now number, uses a call to the pubnub hereNow function.
     * @param displayUsers If true, display a modal of users in room.
     */
    public void hereNow(final boolean displayUsers) {
        this.mPubNub.hereNow(this.channel, new Callback() {
            @Override
            public void successCallback(String channel, Object response) {
                try {
                    JSONObject json = (JSONObject) response;
                    final JSONArray hereNowJSON = json.getJSONArray("uuids");
                    Log.d("JSON_RESP", "Here Now: " + json.toString());
                    final Set<String> usersOnline = new HashSet<String>();
                    usersOnline.add(username);
                    for (int i = 0; i < hereNowJSON.length(); i++) {
                        usersOnline.add(hereNowJSON.getString(i));
                    }
                    ChatActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mChatAdapter.setOnlineNow(usersOnline);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Called at login time, sets meta-data of users' log-in times using the PubNub State API.
     *   Information is retrieved in getStateLogin
     */
    public void setStateLogin(){
        Callback callback = new Callback() {
            @Override
            public void successCallback(String channel, Object response) {
                Log.d("PUBNUB", "State: " + response.toString());
            }
        };
        try {
            JSONObject state = new JSONObject();
            state.put(Constants.STATE_LOGIN, System.currentTimeMillis());
            this.mPubNub.setState(this.channel, this.mPubNub.getUUID(), state, callback);
        }
        catch (JSONException e) { e.printStackTrace(); }
    }

    /**
     * Get state information. Information is deleted when user unsubscribes from channel
     *   so display a user not online message if there is no UUID data attached to the
     *   channel's state
     * @param user
     */
    public void getStateLogin(final String user){
        Callback callback = new Callback() {
            @Override
            public void successCallback(String channel, Object response) {
                if (!(response instanceof JSONObject)) return; // Ignore if not JSON
                try {
                    JSONObject state = (JSONObject) response;
                    final boolean online = state.has(Constants.STATE_LOGIN);
                    final long loginTime = online ? state.getLong(Constants.STATE_LOGIN) : 0;

                    ChatActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!online)
                                Toast.makeText(ChatActivity.this, user + " is not online.", Toast.LENGTH_SHORT).show();
                            else
                                Toast.makeText(ChatActivity.this, user + " logged in since " + ChatAdapter.formatTimeStamp(loginTime), Toast.LENGTH_SHORT).show();

                        }
                    });

                    Log.d("PUBNUB", "State: " + response.toString());
                } catch (JSONException e){ e.printStackTrace(); }
            }
        };
        this.mPubNub.getState(this.channel, user, callback);
    }

    /**
     * Get last 100 messages sent on current channel from history.
     */
    public void history() {
        this.mPubNub.history(this.channel, 100, false, new Callback() {
            @Override
            public void successCallback(String channel, final Object message) {
                try {
                    JSONArray json = (JSONArray) message;
                    final JSONArray messages = json.getJSONArray(0);
                    final List<ChatMessage> chatMsgs = new ArrayList<ChatMessage>();
                    for (int i = 0; i < messages.length(); i++) {
                        try {
                            if (!messages.getJSONObject(i).has("data")) continue;
                            JSONObject jsonMsg = messages.getJSONObject(i).getJSONObject("data");
                            String name = jsonMsg.getString(Constants.JSON_USER);
                            String msg = jsonMsg.getString(Constants.JSON_MSG);
                            long time = jsonMsg.getLong(Constants.JSON_TIME);
                            String ownername = jsonMsg.getString(Constants.JSON_OWNER);
                            ChatMessage chatMsg = new ChatMessage(name, msg, time, ownername);

                            if ((name.equals(selecteduserforchat) && ownername.equals(ChatActivity.this.username))
                                    || (name.equals(ChatActivity.this.username) && ownername.equals(selecteduserforchat))) {

                                finalChatMsg = chatMsg;
                                chatMsgs.add(chatMsg);

                                for (int k = 0; k < User.allUsersOnline.size(); k++) {
                                    User user = User.allUsersOnline.get(k);
                                    if (user.getName().equals(selecteduserforchat)) {
                                        user.setName(selecteduserforchat);
                                        user.setChatMessages(chatMsgs);
                                    }
                                }
                            }
                        } catch (JSONException e) { // Handle errors silently
                            e.printStackTrace();
                        }
                    }
                    ChatActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mChatAdapter.setMessages(chatMsgs);
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void errorCallback(String channel, PubnubError error) {
                Log.d("History", error.toString());
            }
        });
    }

    /**
     *
     * Setup the listview to scroll to bottom anytime it receives a message.
     */
    private void setupAutoScroll() {
        this.mChatAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                mListView.setSelection(mChatAdapter.getCount() - 1);
            }
        });
    }

    /**
     * On message click, display the last time the user logged in.
     */
    private void setupListView() {
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ChatMessage chatMsg = mChatAdapter.getItem(position);
                sendNotification(chatMsg.getUsername());
            }
        });
    }

    /**
     * Publish message to current channel.
     * @param view The 'SEND' Button which is clicked to trigger a sendMessage call.
     */
    public void sendMessage(View view){
        sendChatMessage();
    }

    private void sendChatMessage() {
        String message = mMessageET.getText().toString();
        if (message.equals("")) return;
        mMessageET.setText("");
        ChatMessage chatMsg = new ChatMessage(selecteduserforchat, message, System.currentTimeMillis(), this.username);
        try {
            JSONObject json = new JSONObject();
            json.put(Constants.JSON_USER, chatMsg.getUsername());
            json.put(Constants.JSON_MSG,  chatMsg.getMessage());
            json.put(Constants.JSON_TIME, chatMsg.getTimeStamp());
            json.put(Constants.JSON_OWNER, chatMsg.getChatOwner());
            publish(Constants.JSON_DM, json);
        } catch (JSONException e){ e.printStackTrace(); }
        mChatAdapter.addMessage(chatMsg);
    }

    /**
     * This method will send the chat message
     * @param toUser
     */
    public void sendNotification(String toUser) {
        PnGcmMessage gcmMessage = new PnGcmMessage();
        JSONObject json = new JSONObject();
        try {
            json.put(Constants.GCM_POKE_FROM, this.username);
            json.put(Constants.GCM_CHAT_ROOM, this.channel);
            gcmMessage.setData(json);

            PnMessage message = new PnMessage(this.mPubNub,toUser,new BasicCallback(),gcmMessage);
            message.put("pn_debug",true); // Subscribe to yourchannel-pndebug on console for reports
            message.publish();
        }
        catch (JSONException e) { e.printStackTrace(); }
        catch (PubnubException e) { e.printStackTrace(); }
    }

}
