package com.Go4WorldBusiness.View.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Go4WorldBusiness.Controller.App.ApplicationController;
import com.Go4WorldBusiness.Controller.Interface.OnUserSelectListener;
import com.Go4WorldBusiness.Controller.Network.WebserviceRequest;
import com.Go4WorldBusiness.Model.ChatMessage;
import com.Go4WorldBusiness.Model.Contact;
import com.Go4WorldBusiness.Model.UserContactList;
import com.Go4WorldBusiness.Model.UserList;
import com.Go4WorldBusiness.R;
import com.Go4WorldBusiness.View.Adapter.ChatAdapter;
import com.Utility.Network;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.pubnub.api.Callback;
import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubError;
import com.pubnub.api.PubnubException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import com.Go4WorldBusiness.View.Adapter.UserListAdapter;
import com.Go4WorldBusiness.Controller.Callbacks.BasicCallback;
import com.Go4WorldBusiness.Constants;
import com.Go4WorldBusiness.Model.User;
import com.Go4WorldBusiness.View.ViewHolder.UserListHolder;

/**
 * UserListActivity is where all registered users will be displayed. A RecycleView that is populated by a custom adapter, UserListAdapter.
 * This method extends the AppCompatActivity
 */
public class UserListActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    public String username, password;
    private Pubnub mPubNub;
    private TextView mChannelView;
    private SharedPreferences mSharedPrefs;
    private String channel  = "Go4WorldBusiness";
    private GoogleCloudMessaging gcm;
    private String gcmRegId;
    private UserListAdapter adapter;
    private RecyclerView recyclerView;
    private boolean isHandler=false;
    private Handler mHandler =null;
    private Runnable mUpdateResults =null;
    private Timer timer = null;
    private static final int REQUEST_CHAT = 0;
    UserContactList allContact;
    UserList singleUser;
    private List<Contact> allContacts;
    private List<UserContactList> allContactList;
    private ChatAdapter mChatAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userlist);

        // Init design element objects using its ids
        this.recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        this.mChannelView = (TextView) findViewById(R.id.channel_bar);
        this.mChannelView.setText(this.channel);


        // Get Chat Owner username from Shared preference and navigate to Login screen if username is empty
        mSharedPrefs = getSharedPreferences(Constants.CHAT_PREFS, MODE_PRIVATE);
        if (!mSharedPrefs.contains(Constants.CHAT_USERNAME)){
            Intent toLogin = new Intent(this, LoginActivity.class);
            startActivity(toLogin);
            return;
        }

        this.username = mSharedPrefs.getString(Constants.CHAT_USERNAME, "Anonymous");
        this.password = mSharedPrefs.getString(Constants.CHAT_PASSWORD, "Anonymous");
        // Get channel
        Bundle extras = getIntent().getExtras();
        if (extras != null){
            Log.d("Main-bundle", extras.toString() + " Has Chat: " + extras.getString(Constants.CHAT_ROOM));
            if (extras.containsKey(Constants.CHAT_ROOM))
            {
                this.channel = extras.getString(Constants.CHAT_ROOM);
            }

        }
        allContact = new UserContactList();
        singleUser = new UserList();
        allContacts = new ArrayList<Contact>();
        allContactList = new ArrayList<UserContactList>();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(UserListActivity.this);

        // Initialise pubnub
        ContactWebservice();
        initPubNub();

        // Set UserList Adapter
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        adapter = new UserListAdapter(this, User.allUsersOnline, recyclerView);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        adapter.setOnUserSelectListener(new OnUserSelectListener() {
            @Override
            public void onUserSelect(UserListHolder userHolder, final User user, final int position) {
                String selecteduserforchat = user.getName();
                String selectedUserImage = user.getImage();

                Intent intent = new Intent(UserListActivity.this, ChatActivity.class);
                intent.putExtra("channel", UserListActivity.this.channel);
                intent.putExtra("selectedUsernameForChat", selecteduserforchat);
                intent.putExtra("selectedUserImage", selectedUserImage);
                startActivityForResult(intent, REQUEST_CHAT);
            }
        });

        // Get users available in Online

        Log.d("PUBNUB", "1");
        //getOnlineUsers();
        initRunnable();
        autoCallAllUsers();
    }

    private void ContactWebservice() {
        ApplicationController.getInstance().addToRequestQueue(CallSignInWebService(this.username, this.password), "SignIn");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CHAT) {
            if (resultCode == RESULT_OK) {
                //getOnlineUsers();

                if(data != null){
                    String selectedUserName=data.getStringExtra("NAME");
                    String selectedUserLastMessage=data.getStringExtra("MESSAGE");
                    long selectedUserLastTimeStamp=data.getLongExtra("TIMESTAMP", 0);


                    for (int k = 0; k < User.allUsersOnline.size(); k++) {
                        User user = User.allUsersOnline.get(k);

                        if(user.getName().equals(selectedUserName)){

                            List<ChatMessage> chatMessagesVal = user.getChatMessages();
                            ChatMessage newChat = new ChatMessage(selectedUserLastMessage,selectedUserLastMessage, selectedUserLastTimeStamp,this.username);
                            chatMessagesVal.add(newChat);
                            user.setChatMessages(chatMessagesVal);
                        }

                    }
                }

            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            finish();
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // TODO: Update to store messages in the array.
    }

    /**
     * Might want to unsubscribe from PubNub here and create background service to listen while
     *   app is not in foreground.
     * PubNub will stop subscribing when screen is turned off for this demo, messages will be loaded
     *   when app is opened through a call to history.
     * The best practice would be creating a background service in onStop to handle messages.
     */
    @Override
    protected void onStop() {
        super.onStop();
        if (this.mPubNub != null)
            this.mPubNub.unsubscribeAll();
    }

    /**
     * Instantiate PubNub object if it is null. Subscribe to channel and pull old messages via
     *   history.
     */
    @Override
    protected void onRestart() {
        super.onRestart();
        if (this.mPubNub==null){
            initPubNub();
        } else {
            subscribeWithPresence();
        }
    }

    /**
     * I remove the PubNub object in onDestroy since turning the screen off triggers onStop and
     *   I wanted PubNub to receive messages while the screen is off.
     *
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * Instantiate PubNub object with username as UUID
     *   Then subscribe to the current channel with presence.
     *   Finally, populate the listview with past messages from history
     */
    private void initPubNub(){
        this.mPubNub = new Pubnub(Constants.PUBLISH_KEY, Constants.SUBSCRIBE_KEY);
        this.mPubNub.setUUID(this.username);
        Log.v("UUID_ON", "" + this.mPubNub.getUUID());
        subscribeWithPresence();
        //history();
        gcmRegister();
    }

    /**
     * This method will retrieve all online users from PUBNUB
     */
    public void getOnlineUsers() {
        this.mPubNub.hereNow(this.channel, new Callback() {
            //this.mPubNub.hereNow(true, true, new Callback() {
            @Override
            public void successCallback(String channel, Object response) {

                try {
                    JSONObject json = (JSONObject) response;

                    Log.d("PUBNUB_RESPONSE subs", "" + json);
                    final JSONArray hereNowJSON = json.getJSONArray("uuids");
                    Log.d("PUBNUB hereNowJSON subs", "" + hereNowJSON);

                    for (int i = 0; i < hereNowJSON.length(); i++) {

                        User user2 = new User();
                        user2.setName(hereNowJSON.getString(i));
                        user2.setImage("" + Constants.USER_IMAGE);
                        if (!user2.getName().equals(UserListActivity.this.username))
                            if (!User.allUsersOnline.contains(user2)) {

                                User.allUsersOnline.add(user2);

                                /*List<ChatMessage> userChatList =  user2.getChatMessages();

                                if(userChatList!=null ){
                                    if(!userChatList.isEmpty()){
                                        Log.d("userChatList", ""+userChatList.size());
                                        User.allUsersOnline.add(user2);
                                    }else{
                                        Log.d("userChatList", "1");
                                    }
                                }else{
                                    Log.d("userChatList", "2");
                                }*/
                                if(allContact.getAllContact()!=null) {
                                    for (int k = 0; k < allContact.getAllContact().size(); k++) {
                                        Contact contact = allContact.getAllContact().get(k);
                                        if (contact.getName().equals(user2.getName())) {
                                            contact.setOnlineuser(user2.getName());
                                        }
                                    }
                                }
                                ///singleUser.setAllUser(User.allUsersOnline);
                            } else {
                                Log.d("userChatList", "3");
                                Log.d("PUBNUB", "This user already exists: so replace");
                            }
                    }

                    //history();

                    /*String name = jsonMsg.getString(Constants.JSON_USER);
                    String ownername = jsonMsg.getString(Constants.JSON_OWNER);
                    for (int k = 0; k < User.allUsersOnline.size(); k++) {
                        User user = User.allUsersOnline.get(k);
                            if(user.getName()){
                                user.setName(user.getName());
                                user.setChatMessages();
                            }
                    }*/

                    UserListActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.setOnlineNow(User.allUsersOnline);
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Called at login time, sets meta-data of users' log-in times using the PubNub State API.
     *   Information is retrieved in getStateLogin
     */
    public void setStateLogin(){
        Callback callback = new Callback() {
            @Override
            public void successCallback(String channel, Object response) {
                Log.d("PUBNUB", "State:" + response.toString());
            }
        };
        try {
            JSONObject state = new JSONObject();
            state.put(Constants.STATE_LOGIN, System.currentTimeMillis());
            state.put(Constants.STATE_LOGIN_PASS, "tester");

            this.mPubNub.setState(this.channel, this.mPubNub.getUUID(), state, callback);
        }
        catch (JSONException e) { e.printStackTrace(); }
    }


    /**
     * Subscribe to channel, when subscribe connection is established, in connectCallback, subscribe
     *   to presence, set login time with setStateLogin and update hereNow information.
     * When a message is received, in successCallback, get the ChatMessage information from the
     *   received JSONObject and finally put it into the listview's ChatAdapter.
     * Chat adapter calls notifyDatasetChanged() which updates UI, meaning must run on UI thread.
     */
    public void subscribeWithPresence(){
        Callback subscribeCallback = new Callback() {
            @Override
            public void successCallback(String channel, Object message) {

                /*if (message instanceof JSONObject){
                    try {

                        JSONObject jsonObj = (JSONObject) message;
                        JSONObject json = jsonObj.getJSONObject("data");
                        final String name = json.getString(Constants.JSON_USER);
                        String msg  = json.getString(Constants.JSON_MSG);
                        long time   = json.getLong(Constants.JSON_TIME);
                        final  String ownername = json.getString(Constants.JSON_OWNER);
                        if (ownername.equals(mPubNub.getUUID())) return; // Ignore own messages
                        final ChatMessage chatMsg = new ChatMessage(name, msg, time ,ownername);
                        UserListActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if ((name.equals(name) && ownername.equals(UserListActivity.this.username))
                                        || (name.equals(UserListActivity.this.username) && ownername.equals(name))) {
                                    mChatAdapter.addMessage(chatMsg);
                                    Log.d("subscribeWithPresence", "" + User.allUsersOnline.size());
                                    for (int k = 0; k < User.allUsersOnline.size(); k++) {
                                        User user = User.allUsersOnline.get(k);

                                            user.setChatMessages((List<ChatMessage>)mChatAdapter.getMessages());
                                    }
                                }
                            }
                        });
                    } catch (JSONException e){ e.printStackTrace(); }
                }*/

                Log.d("MAIN_LOGGER_channel", "channel: " + channel);
                Log.d("PUBNUB", "Channel: " + channel + " Msg: " + message.toString());
            }

            @Override
            public void connectCallback(String channel, Object message) {
                Log.d("Subscribe", "Connected! " + message.toString());
                Log.d("PUBNUB", "2");
                getOnlineUsers();
                setStateLogin();
            }
        };
        try {
            mPubNub.subscribe(this.channel, subscribeCallback);
            presenceSubscribe();
        } catch (PubnubException e){ e.printStackTrace(); }
    }

    /**
     * Subscribe to presence. When user join or leave are detected, update the hereNow number
     *   as well as add/remove current user from the chat adapter's userPresence array.
     *   This array is used to see what users are currently online and display a green dot next
     *   to users who are online.
     */

    public void presenceSubscribe()  {
        Callback callback = new Callback() {
            @Override
            public void successCallback(String channel, Object response) {
                Log.i("PN-pres", "Pres: " + response.toString() + " class: " + response.getClass().toString());
                if (response instanceof JSONObject){
                    JSONObject json = (JSONObject) response;
                    Log.d("PN-main","Presence: " + json.toString());
                    try {
                        final int occ = json.getInt("occupancy");
                        final String user = json.getString("uuid");
                        final String action = json.getString("action");
                        UserListActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //adapter.userPresence(user, action);
                            }
                        });
                    } catch (JSONException e){ e.printStackTrace(); }
                }
            }

            @Override
            public void errorCallback(String channel, PubnubError error) {
                Log.d("Presence", "Error: " + error.toString());
            }
        };
        try {
            this.mPubNub.presence(this.channel, callback);
        } catch (PubnubException e) { e.printStackTrace(); }
    }


    /**
     * Get last 100 messages sent on current channel from history.
     */
    public void history() {
        this.mPubNub.history(this.channel, 100, false, new Callback() {
            @Override
            public void successCallback(String channel, final Object message) {
                try {
                    JSONArray json = (JSONArray) message;
                    final JSONArray messages = json.getJSONArray(0);
                    final List<ChatMessage> chatMsgs = new ArrayList<ChatMessage>();
                    Log.d("messages",""+json);
                    for (int i = 0; i < messages.length(); i++) {
                        try {
                            if (!messages.getJSONObject(i).has("data")) continue;
                            JSONObject jsonMsg = messages.getJSONObject(i).getJSONObject("data");
                            String name = jsonMsg.getString(Constants.JSON_USER);
                            String msg = jsonMsg.getString(Constants.JSON_MSG);
                            long time = jsonMsg.getLong(Constants.JSON_TIME);
                            String ownername = jsonMsg.getString(Constants.JSON_OWNER);
                            ChatMessage chatMsg = new ChatMessage(name, msg, time, ownername);



                            if ((name.equals(name) && ownername.equals(UserListActivity.this.username))
                                    || (name.equals(UserListActivity.this.username) && ownername.equals(name))) {

                                chatMsgs.add(chatMsg);

                                Log.d("History", "" +User.allUsersOnline.size());


                                for (int k = 0; k < User.allUsersOnline.size(); k++) {
                                    User user = User.allUsersOnline.get(k);
                                        //user.setName(user.getName());
                                        user.setChatMessages(chatMsgs);
                                }
                            }
                        } catch (JSONException e) { // Handle errors silently
                            e.printStackTrace();
                        }
                    }
                    UserListActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //mChatAdapter.setMessages(chatMsgs);
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void errorCallback(String channel, PubnubError error) {
                Log.d("History", error.toString());
            }
        });
    }
    /**
     * GCM Functionality.
     * In order to use GCM Push notifications you need an API key and a Sender ID.
     * Get your key and ID at - https://developers.google.com/cloud-messaging/
     */
    private void gcmRegister() {
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            try {
                gcmRegId = getRegistrationId();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (gcmRegId.isEmpty()) {
                registerInBackground();
            } else {
                //Toast.makeText(this,"Registration ID already exists: " + gcmRegId,Toast.LENGTH_SHORT).show();
            }
        } else {
            Log.e("GCM-register", "No valid Google Play Services APK found.");
        }
    }

    /**
     * This method will check Play services enabled or not
     * @return
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, Constants.PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.e("GCM-check", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * This method will store registration id temporarily in Shared Preference
     * @param regId
     */
    private void storeRegistrationId(String regId) {
        SharedPreferences prefs = getSharedPreferences(Constants.CHAT_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constants.GCM_REG_ID, regId);
        editor.apply();
    }

    /**
     * This method will get registration id from PubNub
     * @return
     */
    private String getRegistrationId() {
        SharedPreferences prefs = getSharedPreferences(Constants.CHAT_PREFS, Context.MODE_PRIVATE);
        return prefs.getString(Constants.GCM_REG_ID, "");
    }

    /**
     * This method will send registration id in PubNub
     * @param regId
     */
    private void sendRegistrationId(String regId) {
        this.mPubNub.enablePushNotificationsOnChannel(this.username, regId, new BasicCallback());
    }

    /**
     * This method will refresh every 10 seconds to get list of users
     */
    private void autoCallAllUsers() {

        if(Network.haveInternet(UserListActivity.this))
        {
            isHandler=true;
            mHandler = new Handler();
            // Create runnable for posting
            mUpdateResults = new Runnable() {
                public void run() {
                    Log.d("PUBNUB", "3");

                    //subscribeWithPresence();
                    //history();

                    getOnlineUsers();

                    adapter.notifyDataSetChanged();
                }
            };
            int delay = 50000; // delay for 5 sec.
            int period = 5000; // repeat every 5 sec.
            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    if(mHandler!=null)
                        mHandler.post(mUpdateResults);
                }
            }, delay, period);
        }
    }

    private void initRunnable() {
        mUpdateResults = new Runnable() {
            public void run() {
                Log.d("PUBNUB", "4");
                getOnlineUsers();
            }
        };
    }

    /**
     * This method will invoke RegisterTask in background
     */
    private void registerInBackground() {
        new RegisterTask().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_caht) {
        }
        if (id == R.id.nav_cantact) {

            //ContactWebservice();

            Intent intent = new Intent(UserListActivity.this, ContactListActivity.class);
            intent.putExtra("channel", UserListActivity.this.channel);
            intent.putExtra("Contact", allContact);
            intent.putExtra("UserName", singleUser);
            startActivityForResult(intent, REQUEST_CHAT);

        }
        if (id == R.id.nav_log_out) {
            // Handle the log out action
            Exit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void Exit() {

        AlertDialog.Builder csDialog = new AlertDialog.Builder(UserListActivity.this);
        csDialog.setMessage("" + getResources().getString(R.string.alt_log_out));
        csDialog.setPositiveButton(getResources().getString(R.string.alt_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                UserListActivity.this.mPubNub.unsubscribeAll();
                SharedPreferences.Editor edit = mSharedPrefs.edit();
                edit.remove(Constants.CHAT_USERNAME);
                edit.apply();

                Intent intent = new Intent(UserListActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        csDialog.setNegativeButton(getResources().getString(R.string.alt_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        csDialog.show();
    }

    /**
     * This inner class will register the device token in GCM for notification
     */
    private class RegisterTask extends AsyncTask<Void, Void, String>{
        @Override
        protected String doInBackground(Void... params) {
            String msg="";
            try {
                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(UserListActivity.this);
                }
                gcmRegId = gcm.register(Constants.GCM_SENDER_ID);
                msg = "Device registered, registration ID: " + gcmRegId;
                sendRegistrationId(gcmRegId);
                storeRegistrationId(gcmRegId);
                Log.i("GCM-register", msg);
            } catch (IOException e){
                e.printStackTrace();
            }
            return msg;
        }
    }

    /**
     * This method will request the webservice and retrive response.
     * @param userName
     * @return
     */
    private WebserviceRequest CallSignInWebService(final String userName, String password ) {

        String fullRequestString = Constants.DEMO_URL+Constants.API_REQUEST_TAG_TYPE+"="+Constants.API_REQUEST_LOGIN +"&"+Constants.API_REQUEST_USRNAME+"="+userName+"&"+Constants.API_REQUEST_PASSWORD+"="+password;

        WebserviceRequest jsObjRequest = new WebserviceRequest(Request.Method.GET, fullRequestString, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    try {
                        String responseCode = response.getString(Constants.API_RESPONSE_CODE);
                        String message = response.getString(Constants.API_RESPONSE_MESSAGE);
                        if (responseCode.equals("1")) {
                            Log.v("Service Response", "" + response.toString());
                            parseContactDetailsJsonData(response ,userName);
                            new Thread() {
                                public void run() {
                                    UserListActivity.this.runOnUiThread(new Runnable() {
                                        public void run() {
                                            Log.v("Service Response", "Authentication success");

                                        }
                                    });
                                }
                            }.start();
                        }
                        else {
                            Toast.makeText(UserListActivity.this, "", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError response) {
                Log.d("Response: ", response.toString());
                Log.v("Error", "" + response.getMessage());
            }
        });

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        jsObjRequest.setShouldCache(false);

        return jsObjRequest;
    }

    private void parseContactDetailsJsonData(JSONObject jsonObject, String username) {

        try{
            /* Getting json */
            allContacts.clear();

            JSONArray jsonContactArray = jsonObject.getJSONArray(Constants.API_RESPONSE_CONTACT_USER);

            for (int jloop = 0; jloop < jsonContactArray.length(); jloop++) {

                JSONObject jsonContactListObj = jsonContactArray.getJSONObject(jloop);
                Contact singleList = new Contact();
                String getName = jsonContactListObj.getString(Constants.API_RESPONSE_CONTACT_NAME);
                singleList.setName(getName);
                String getPassword = jsonContactListObj.getString(Constants.API_RESPONSE_CONTACT_PASSWORD);
                singleList.setPassword(getPassword);
                String getImage = jsonContactListObj.getString(Constants.API_RESPONSE_CONTACT_IMAGE);
                singleList.setImage(Constants.USER_IMAGE);

                allContacts.add(singleList);

                Log.v("allContacts",""+singleList.getName());

                if(!username.equals(singleList.getName())){
                    allContact.setAllContact(allContacts);
                    allContactList.add(allContact);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
