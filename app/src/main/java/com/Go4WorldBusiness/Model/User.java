package com.Go4WorldBusiness.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


/**
 * Shenll Technology Solutions
 * UserList is used to hold users information that is transmitted using PubNub.
 * A user in this app has a name, image, chatMessages.
 */
public class User implements Parcelable {

    private String name;
    private String image;
    private List<ChatMessage> chatMessages;

    public static final List<User> allUsersOnline = new ArrayList<>();

    /**
     * Constructor
     */
    public User(){

    }

    /**
     * Constructor
     * @param name
     * @param image
     * @param chatMessages
     */
    public User(String name, String image, ArrayList<ChatMessage> chatMessages) {
        this.name = name;
        this.image = image;
        this.chatMessages = chatMessages;
    }
    /**
     * This method will set chat user name.
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * This method will return chat user name.
     * @return
     */
    public String getName() {
        return name;
    }
    /**
     * This method will set chat user image.
     */
    public void setImage(String image) {
        this.image = image;
    }
    /**
     * This method will return chat user image.
     * @return
     */
    public String getImage() {
        return image;
    }
    /**
     * This method will set list of chat user messages.
     */
    public void setChatMessages(List<ChatMessage> chatMessages) {
        this.chatMessages = chatMessages;
    }
    /**
     * This method will return list of chat user messages.
     * @return
     */
    public List<ChatMessage> getChatMessages() {
        return chatMessages;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(image);
        dest.writeTypedList(chatMessages);
    }

    // Creator
    public static final Creator<User> CREATOR = new Creator<User>() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    // "De-parcel object
    public User(Parcel in) {
        name = in.readString();
        image = in.readString();
        chatMessages = in.createTypedArrayList(ChatMessage.CREATOR);
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof User)) {
            return false;
        }

        User that = (User) other;

        // Custom equality check here.
        return this.name.equals(that.name)
                && this.image.equals(that.image);
    }
}