package com.Go4WorldBusiness.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Shenll Technology Solutions
 * ChatMessage is used to hold information that is transmitted using PubNub.
 * A message in this app has a username, message, timestamp and chatowner.
 */
public class ChatMessage implements Parcelable{

    private String username;
    private String message;
    private long timeStamp;
    private String chatOwner;

    /**
     * Constructor
     * @param username
     * @param message
     * @param timeStamp
     * @param chatOwner
     */
    public ChatMessage(String username, String message, long timeStamp, String chatOwner){
        this.username  = username;
        this.message   = message;
        this.timeStamp = timeStamp;
        this.chatOwner = chatOwner;
    }

    /**
     * This method will return username of a person with whom chat owner is chatting.
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     * This method will return chat message.
     * @return
     */
    public String getMessage() {
        return message;
    }

    /**
     * This method will return time when chat message received.
     * @return
     */
    public long getTimeStamp() {
        return timeStamp;
    }

    /**
     * This method will return chat ownername.
     * @return
     */
    public String getChatOwner() {
        return chatOwner;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(message);
        dest.writeString(Long.toString(timeStamp));
        dest.writeString(chatOwner);
    }

    // Creator
    public static final Parcelable.Creator<ChatMessage> CREATOR
            = new Parcelable.Creator<ChatMessage>() {
        public ChatMessage createFromParcel(Parcel in) {
            return new ChatMessage(in);
        }

        public ChatMessage[] newArray(int size) {
            return new ChatMessage[size];
        }
    };

    // "De-parcel object
    public ChatMessage(Parcel in) {
        username = in.readString();
        message = in.readString();
        timeStamp = in.readLong();
        chatOwner = in.readString();
    }
}
