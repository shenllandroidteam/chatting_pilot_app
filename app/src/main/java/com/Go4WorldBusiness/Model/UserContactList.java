package com.Go4WorldBusiness.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


/**
 * Shenll Technology Solutions
 * UserContactList is used to hold users information that is transmitted using PubNub.
 * A Contact in this app has a allContact.
 */
public class UserContactList implements Parcelable {

    public List<Contact> allContact;

    /**
     * Constructor
     */
    public UserContactList(){

    }

    /**
     * Constructor
     * @param allContact
     */
    public UserContactList(ArrayList<Contact> allContact) {

        this.allContact = allContact;
    }

    /**
     * This method will set list of chat user messages.
     */
    public void setAllContact(List<Contact> allContact) {
        this.allContact = allContact;
    }
    /**
     * This method will return list of chat user messages.
     * @return
     */
    public List<Contact> getAllContact() {
        return allContact;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
       dest.writeTypedList(allContact);
    }

    // Creator
    public static final Creator<UserContactList> CREATOR
            = new Creator<UserContactList>() {
        public UserContactList createFromParcel(Parcel in) {
            return new UserContactList(in);
        }

        public UserContactList[] newArray(int size) {
            return new UserContactList[size];
        }
    };

    // "De-parcel object
    public UserContactList(Parcel in) {
        allContact = in.createTypedArrayList(Contact.CREATOR);
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof UserContactList)) {
            return false;
        }

        UserContactList that = (UserContactList) other;

        // Custom equality check here.
        return false;
    }
}