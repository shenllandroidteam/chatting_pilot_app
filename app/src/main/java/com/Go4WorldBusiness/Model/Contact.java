package com.Go4WorldBusiness.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


/**
 * Shenll Technology Solutions
 * Contact is used to hold users information that is transmitted using PubNub.

 * A user in this app has a name, password, image.
 */
public class Contact implements Parcelable {

    private String name;
    private String password;
    private String image;

    private String onlineuser;

    public static final List<Contact> allContacts = new ArrayList<>();

    /**
     * Constructor
     */
    public Contact(){

    }

    /**
     * Constructor
     * @param name
     * @param image
     * @param password
     * @param onlineuser
     */
    public Contact(String name, String password, String image, String onlineuser) {
        this.name = name;
        this.password = password;
        this.image = image;
        this.onlineuser = onlineuser;
    }
    /**
     * This method will set chat user name.
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * This method will return chat user name.
     * @return
     */
    public String getName() {
        return name;
    }
    /**
     * This method will set chat user password.
     */
    public void setPassword(String password) {
        this.password = password;
    }
    /**
     * This method will return chat user password.
     * @return
     */
    public String getPassword() {
        return password;
    }
    /**
     * This method will set chat user image.
     */
    public void setImage(String image) {
        this.image = image;
    }
    /**
     * This method will return chat user image.
     * @return
     */
    public String getImage() {
        return image;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    public String getOnlineuser() {
        return onlineuser;
    }

    public void setOnlineuser(String onlineuser) {
        this.onlineuser = onlineuser;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof User)) {
            return false;
        }

        Contact that = (Contact) other;

        // Custom equality check here.
        return this.name.equals(that.name) && this.password.equals(that.password)
                && this.image.equals(that.image)&& this.onlineuser.equals(that.onlineuser);
    }



    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(password);
        dest.writeString(image);
        dest.writeString(onlineuser);
    }

    // Creator
    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    // "De-parcel object
    public Contact(Parcel in) {
        name = in.readString();
        password = in.readString();
        image = in.readString();
        onlineuser = in.readString();
    }


}