package com.Go4WorldBusiness.Model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;


/**
 * Shenll Technology Solutions
 * UserList is used to hold users information that is transmitted using PubNub.
 * A user in this app has a name, image, chatMessages.
 */
public class UserList implements Parcelable {

    private List<User> allUser;

    /**
     * Constructor
     */
    public UserList(){

    }

    /**
     * Constructor
     * @param allUser
     */
    public UserList(ArrayList<User> allUser) {
        this.allUser = allUser;
    }

    /**
     * This method will set list of user.
     */
    public void setAllUser(List<User> allUser) {
        this.allUser = allUser;
    }
    /**
     * This method will return list of user.
     * @return
     */
    public List<User> getAllUser() {
        return allUser;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
         dest.writeTypedList(allUser);
    }

    // Creator
    public static final Creator<UserList> CREATOR = new Creator<UserList>() {
        public UserList createFromParcel(Parcel in) {
            return new UserList(in);
        }

        public UserList[] newArray(int size) {
            return new UserList[size];
        }
    };

    // "De-parcel object
    public UserList(Parcel in) {
        allUser = in.createTypedArrayList(User.CREATOR);
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof UserList)) {
            return false;
        }

        UserList that = (UserList) other;

        // Custom equality check here.
        return true;
    }
}